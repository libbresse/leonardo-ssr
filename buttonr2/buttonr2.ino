#include <PinChangeInt.h>

#define PINe5 8     // the number of the pushbutton pin
#define ledPin 3     // the number of the LED pin

// variables will change:
volatile boolean bState;         // variable for reading the pushbutton status
volatile boolean bBounce;         // variable for removing bounce
int timerPin = A0;    // select the input pin for the timer
int intensPin = A1;    // select the input pin for the intensity
int timerValue = 0;  // variable to store the value coming from the sensor
int intensValue = 0;  // variable to store the value coming from the sensor


void state() {
  if(bBounce==true){
    bState = !bState; 
  }
  bBounce = false;
}



void setup() {
  // initialize the LED pin as an output:
    //Attach the interrupt to the input pin and monitor for ANY Change
  Serial.begin(9600);
//  while (!Serial) {
//    ; // wait for serial port to connect. Needed for Leonardo only
//  }
  bState = false;  
  Serial.println("pinnnn in");
  // initialize the pushbutton pin as an input:
  pinMode(ledPin, OUTPUT);digitalWrite(ledPin, LOW);
  pinMode(PINe5, INPUT);digitalWrite(PINe5, HIGH);
//  Serial.println("pinnnn in2");
//  delay(50);  
  //PCintPort::attachInterrupt(PIN5, state, FALLING);
  PCintPort::attachInterrupt(PINe5, &state, FALLING);
//  Serial.println("pinnnn in3");
  
  
}



void loop(){
  int delayOn;
  int cycleTime = 250;
  while(1){// read the state of the pushbutton value:
    // if it is, the buttonState is HIGH:
    if (bState == true) {
    
      //timerValue = analogRead(timerPin)*5;
      for (int i=0; i <= 5000; i++){ 
        intensValue = analogRead(intensPin);
        int cycleTime = analogRead(timerPin)*3;
        digitalWrite(ledPin, HIGH);
        delayOn = intensValue/10; 
        delay(delayOn);
        //delay(300);
        digitalWrite(ledPin, LOW);
        delay(80 + cycleTime-delayOn);
//        Serial.println(delayOn);
        delay(2);
//        Serial.println(timerValue);
        if (bState == false) {
          i = 6000;
        }
        bBounce = true;
    }
      
      bState= false;
      
    } 
    else {
      // turn LED off:
//      Serial.println("intense");
//      Serial.println(analogRead(intensPin));
//      Serial.println("time");
//      Serial.println(analogRead(timerPin));
      delay(600);
      bBounce = true;  
    }
  }
}
